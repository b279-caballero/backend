// console.log("HELLO JAY");

async function fetchData(){
	
	let result = await fetch("https://jsonplaceholder.typicode.com/todos");
	let json = await result.json();
}

fetchData();


fetch("https://jsonplaceholder.typicode.com/todos")
.then(res => res.json())
.then(title => {
	let titleMap = title.map(dataToMap => 
		  dataToMap.title
	);
	console.log(titleMap);
});



fetch("https://jsonplaceholder.typicode.com/todos/1")
  .then(response => response.json())
  .then(json => {
  	console.log(json.title)
  	console.log(json.completed)
  });
  

fetch("https://jsonplaceholder.typicode.com/todos", {
	method: "POST",
	headers: {"Content-Type" : "application/json"},
	body: JSON.stringify({
		title: "activity"
	})
})
.then(res => res.json())
.then(json => console.log(json));


fetch("https://jsonplaceholder.typicode.com/todos/1", {
	method: "PUT",
	headers: {"Content-Type" : "application/json"},
	body: JSON.stringify({
		title: "Hello",
		userId: 1,
		description: "learns api",
		status: "incomplete",
		dateCompleted: null
	})
})
.then(res => res.json())
.then(json => console.log(json));

// PATCH
fetch("https://jsonplaceholder.typicode.com/todos/1", {
	method: "PUT",
	headers: {"Content-Type" : "application/json"},
	body: JSON.stringify({
		status: "complete",
		dateCompleted: "5-3-2023"
	})
})
.then(res => res.json())
.then(json => console.log(json));


fetch("https://jsonplaceholder.typicode.com/todos/1", {
	method: "DELETE"
}
	.then(res => res.json())
	.then(json => console.log(json));
