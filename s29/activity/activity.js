
// FIND USERS WITH S AND D
db.users.find({$or: [{ firstName: {$regex: "S", $options: "s"}}, { lastName: {$regex: "D"}}]},{firstName: 1,lastName: 1,_id: 0});

// FIND USERS HR
db.users.find({ $and: [{department: "HR"}, {age: {$gte: 70}}] });

// FIND USERS WITH LETTER E
db.users.find({ $and: [{firstName: {$regex: "e", $options: "E"}}, {age: {$lte: 30}}]});
