// 1. What directive is used by Node.js in loading the modules it needs?
	Require


// 2. What Node.js module contains a method for server creation?
	http module


// 3. What is the method of the http object responsible for creating a server using Node.js?
	createServer()


// 4. What method of the response object allows us to set status codes and content types?
	res.writeHead()


// 5. Where will console.log() output its contents when run in Node.js?
	 the output will be printed to the console or terminal where you executed the Node.js process


// 6. What property of the request object contains the address's endpoint?
	 req.url
