
const http = require("http");

const port = 3000;

const server = http.createServer((request, response) => {

	if(request.url == "/login"){
		response.writeHead(200, {"Content-Type" : "text/plain"});
		response.end("Welcome to login page!");
	}else{
		response.writeHead(404, {"Content-Type" : "text/plain"});
		response.end("Error");
	}
});

server.listen(port);
console.log(`Server is successfully running at localhost:${port}`);

// node index.js
// npm install -g nodemon
// nodemon routes.js